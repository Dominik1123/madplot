import unittest

from madplot.madx.builder import Command, DerivedCommand, E, Script, Sequence, Track, Placeholder
from madplot.madx.builder import AdvancedControl


class TestCommand(unittest.TestCase):
    def test_keyword(self):
        self.assertEqual(Command('example').keyword, 'example')
        self.assertEqual(str(Command('example')), 'example')

    def test_arguments(self):
        self.assertEqual(Command('test', example='value')['example'], 'value')
        self.assertEqual(Command('test', example='value', other='data').attributes, dict(example='value', other='data'))
        self.assertEqual(str(Command('test', example='value', other='data')), 'test, example = value, other = data')

    def test_modify_argument(self):
        c = Command('test', key='value')
        c['key'] = 'new'
        self.assertEqual(c['key'], 'new')
        self.assertEqual(str(c), 'test, key = new')

    def test_evaluated_expressions(self):
        self.assertEqual(str(Command('test', key=E('value'))), 'test, key := value')

    def test_derived(self):
        self.assertEqual(str(DerivedCommand(Command('c1', key='val'), 'c2', other='data')), 'c2, other = data')
        self.assertEqual(str(DerivedCommand(Command('c1', key='val'), 'c2', key='new')), 'c2, key = new')


class TestScript(unittest.TestCase):
    def test_unlabeled(self):
        s = Script()
        s += 'test'
        self.assertEqual(s.elements, [(None, 'test')])
        self.assertEqual(str(s), 'test;')
        s += 'other'
        self.assertEqual(s.elements, [(None, 'test'), (None, 'other')])
        self.assertEqual(str(s), 'test;\nother;')

    def test_unlabeled_old_syntax(self):
        s = Script()
        s._ = 'test'
        s._ = 'other'
        self.assertEqual(s.elements, [(None, 'test'), (None, 'other')])
        self.assertEqual(str(s), 'test;\nother;')

    def test_labeled(self):
        s = Script()
        s['label'] = 'value'
        s += 'unlabeled'
        s['other_label'] = 'other_value'
        self.assertEqual(dict(**s.definitions), dict(label='value', other_label='other_value'))
        self.assertEqual(s.elements, [('label', 'value'), (None, 'unlabeled'), ('other_label', 'other_value')])
        self.assertEqual(str(s), 'label = value;\nunlabeled;\nother_label = other_value;')

    def test_commands(self):
        s = Script()
        s['label'] = s.command(key='value')
        s.inplace_(foo='bar')
        self.assertEqual(str(s), 'label: command, key = value;\ninplace, foo = bar;')

    def test_comment(self):
        s = Script()
        s += '// Test'
        s += '/* Comment */'
        self.assertEqual(str(s), '// Test\n/* Comment */')

    def test_modify_command(self):
        s = Script()
        s['label'] = s.command(key='value')
        s['label']['key'] = 'new'
        self.assertEqual(s['label']['key'], 'new')
        self.assertEqual(str(s['label']), 'command, key = new')
        self.assertEqual(str(s), 'label: command, key = new;')
        s['label']['new_arg'] = 'new_val'
        self.assertEqual(str(s), 'label: command, key = new, new_arg = new_val;')


class TestBlock(unittest.TestCase):
    def test_implementations(self):
        self.assertEqual(str(Sequence()), f'sequence;\n{4*" "}\nendsequence;')
        self.assertEqual(str(Track()), f'track;\n{4*" "}\nendtrack;')

    def test_arguments(self):
        self.assertEqual(str(Sequence(key='value')), f'sequence, key = value;\n{4*" "}\nendsequence;')

    def test_add_elements(self):
        with Sequence() as s:
            s += 'unlabeled'
            s.inplace_(some='data')
        self.assertEqual(str(s), f'sequence;\n{4*" "}unlabeled;\n{4*" "}inplace, some = data;\nendsequence;')

    def test_setitem(self):
        s = Sequence()
        s['arg'] = 'val'
        s['label'] = s.command(key='value')
        self.assertEqual(str(s), f'sequence, arg = val;\n{4*" "}label: command, key = value;\nendsequence;')


class TestPlaceholder(unittest.TestCase):
    def test_name(self):
        self.assertEqual(Placeholder('test').keyword, '{test}')

    def test_interpolation(self):
        s = Script()
        s['label'] = Placeholder('test')
        s = str(s).format(test=str(Command('specific', some='data')))
        self.assertEqual(s, 'label: specific, some = data;')

    def test_interpolation_with_placeholder_args(self):
        s = Script()
        s['label'] = Placeholder('test', place='holder')
        s = str(s).format(test=str(Command('specific', some='data')))
        self.assertEqual(s, 'label: specific, some = data, place = holder;')

    def test_interpolation_for_script_with_list_arguments(self):
        s = Script()
        s.command_(arg=[1, 2, 3])
        s['label'] = Placeholder('test', place='holder')
        s = str(s)
        self.assertEqual(s, 'command, arg = {{1, 2, 3}};\nlabel: {test}, place = holder;')
        s = s.format(test=str(Command('specific', some='data')))
        self.assertEqual(s, 'command, arg = {1, 2, 3};\nlabel: specific, some = data, place = holder;')


# noinspection PyStatementEffect
class TestAdvancedControlCursor(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        AdvancedControl.Cursor.activate()

    @classmethod
    def tearDownClass(cls):
        AdvancedControl.Cursor.deactivate()

    def test_access(self):
        s = Script()
        s['a'] = 1
        s['b'] = 2
        s['c'] = 3
        self.assertEqual(s['a'].item, 1)
        self.assertEqual(s['b'].item, 2)
        self.assertEqual(s['c'].item, 3)
        self.assertEqual(s[0].item, 1)
        self.assertEqual(s[1].item, 2)
        self.assertEqual(s[2].item, 3)

    def test_advanced_access(self):
        s = Script()
        s.test_(arg='test 1')
        s.other_(arg='other 1')
        s.test_(arg='test 2')
        s.other_(arg='other 2')
        s.test_(arg='test 3')
        s.other_(arg='other 3')
        self.assertEqual(s['test']['arg'], 'test 1')
        self.assertEqual(s['test[0]']['arg'], 'test 1')
        self.assertEqual(s['test[1]']['arg'], 'test 2')
        self.assertEqual(s['test[2]']['arg'], 'test 3')

    def test_get_and_setitem(self):
        s = Script()
        s['cmd'] = s.command(key='val')
        s['cmd']['key'] = 'new'
        self.assertEqual(s['cmd']['key'], 'new')
        self.assertEqual(str(s['cmd']), 'command, key = new')
        self.assertEqual(str(s), 'cmd: command, key = new;')

    def test_get_and_setattr(self):
        s = Script()
        t = Script()
        s['t'] = t
        t['cmd'] = t.command(key='val')
        self.assertIsInstance(s['t'].cmd(), DerivedCommand)
        cursor = s['t']
        cursor._ = 'unlabeled'
        self.assertEqual(str(s['t']), 'cmd: command, key = val;\nunlabeled;')

    def test_offset(self):
        s = Script()
        s['a'] = 1
        s['b'] = 2
        s['c'] = 3
        self.assertEqual((s['a'] >> 1).item, 2)
        self.assertEqual((s['a'] >> 2).item, 3)
        self.assertEqual((s['b'] << 1).item, 1)
        self.assertEqual((s['b'] >> 1).item, 3)
        self.assertEqual((s['c'] << 1).item, 2)
        self.assertEqual((s['c'] << 2).item, 1)
        with self.assertRaises(ValueError):
            s['a'] << 1
        with self.assertRaises(ValueError):
            s['c'] >> 1

    def test_delete(self):
        s = Script()
        s['a'] = 1
        s['b'] = 2
        s['c'] = 3
        ~s['b']
        self.assertEqual(str(s), 'a = 1;\nc = 3;')

    def test_modify(self):
        s = Script()
        s['a'] = 1
        s['b'] = 2
        s['c'] = 3
        s['b'] == ('b', 'new')
        self.assertEqual(str(s), 'a = 1;\nb = new;\nc = 3;')

    def test_insert(self):
        s = Script()
        s['a'] = 1
        s['d'] = 4
        s['g'] = 7
        s['a'] > ('b', 2)
        s['d'] < ('c', 3)
        s['d'] > ('e', 5)
        s['g'] < ('f', 6)
        self.assertEqual(str(s), 'a = 1;\nb = 2;\nc = 3;\nd = 4;\ne = 5;\nf = 6;\ng = 7;')
        s['a'] < 0
        s['g'] > 8
        self.assertEqual(str(s), '0;\na = 1;\nb = 2;\nc = 3;\nd = 4;\ne = 5;\nf = 6;\ng = 7;\n8;')


if __name__ == '__main__':
    unittest.main()
