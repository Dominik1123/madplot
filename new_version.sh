#!/bin/bash

sed -ri "s/version = '[0-9]+[.][0-9]+([.][0-9]+)?'/version = '$1'/" madplot/__init__.py && echo "New version: $1"
git add madplot/__init__.py
git commit -m "New version: $1"
git tag "$1"
